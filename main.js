var sql = require('mssql');

var config = {
    server : '10.10.14.85',
    database : 'Chinook',
    user : 'sa',
    password : '\'',
    port : 14330
};

// var config = {
//     server : 'thinema.iptime.org',
//     database : 'Chinook',
//     user : 'ChinookOwner',
//     password : '1',
//     port : 3423
// };

// sql.connect(config, err => {
//     var lower = 1;
//     var upper = 2;
//     var request = new sql.Request();
//     request.stream = true;

//     var query = 'select top 3 * from Track where UnitPrice between ';
//     query += lower;
//     query += ' and ';
//     query += upper;

//     request.query(query);

//     request.on('row', row => {
//         console.log(`Name of ${row.Name} is ${row.UnitPrice}`);
//     });
// });

sql.connect(config, err => {
    var request = new sql.Request();

    request.input('Lower', sql.Numeric, 1);
    request.input('Upper', sql.Numeric, 2);

    request.execute(
        'Track_SelectByUnitPrice', 
        (err, result, returnValue) => {
            console.log(result.rowsAffected);
            result.recordset.forEach(row => {
                console.log(`Name of ${row.Name} is ${row.UnitPrice}`);
            }, this);
    });
});